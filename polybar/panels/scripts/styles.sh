#!/usr/bin/env bash

DIR="$HOME/.config/polybar/panels"

change_panel() {
	# replace config with selected panel
	cat "$DIR"/panel/"${panel}.ini" > "$DIR"/config.ini

	# Change wallpaper
	feh --bg-fill "$DIR"/wallpapers/"$bg"
	
	# Restarting polybar
	polybar-msg cmd restart
}

if  [[ "$1" = "--elight" ]]; then
	panel="elementary"
	bg="odin.jpg"
	change_panel

elif  [[ "$1" = "--edark" ]]; then
	panel="elementary_dark"
	bg="odin-dark.jpg"
	change_panel

else
	cat <<- _EOF_
	No option specified, Available options:
	--elight   --edark
	_EOF_
fi
