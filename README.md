# Elementary polybar theme dark & light options for i3-wm


## Preview

## e-dark
![dark](/preview/e-dark.png)
<br />
## e-light
![light](/preview/e-light.png)
<br />


## Fonts
- **Fira Sans**
- **Font Awesome 5 Free**
- **Font Awesome 5 Brands**
- **Iosevka Nerd Font**

### Other prerequisites for polybar module
* `io.elementary.music, vlc, spotify`
* `cava`
* `i3lock, betterlockscreen`

## Details
- **Distro** archlabs ;)
- **WM** i3wm
- **Panel** Polybar
- **Terminal** Alacritty & urxvt
- **Music Player** io.elementary.music & vlc,
- **Text Editor** vim
- **File Manager** Ranger, with w3m image previewer
- **Alternative File Manager** Thunar
- **Screen Recorder** simplescreenrecorder
- **Program Launcher** rofi
- **Info Fetcher** Neofetch
- **GTK Theme** Fluent-dark
- **Icons** Fuent-grey-dark
- **CLI Shell Intepreter** fish
- **Compositor** picom
- **Notification Daemon** dunst

## add this line to your ~/.config/i3/config

## exec --no-startup-id ~/.config/polybar/panels/polybar-wrapper.sh &




- ** Special Thanks
- ** adi1090x: [polybar themes](https://github.com/adi1090x/polybar-themes)

